/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/

document.addEventListener('DOMContentLoaded', () => {
  const root = document.createElement('div');
    root.id = 'root';
  
    const form1 = document.createElement('form');

    const nameInput = document.createElement('input');
    const ageInput = document.createElement('input');
    const passInput = document.createElement('input');
    const generateBtn = document.createElement('button');

    nameInput.name = 'name';
    ageInput.name = 'age';
    passInput.name = 'password';

    generateBtn.innerText = "Generate";

    generateBtn.addEventListener( 'click', (e) => {
      e.preventDefault();
      let obj = {};

      obj.name = nameInput.value;
      obj.age = ageInput.value;
      obj.password = passInput.value;

      console.log( JSON.stringify(obj) );
    });


    form1.appendChild( nameInput );
    form1.appendChild( ageInput );
    form1.appendChild( passInput );
    form1.appendChild( generateBtn );

    const form2 = document.createElement('form');

    const inputStr = document.createElement('input');
    const parseBtn = document.createElement('button');

    parseBtn.innerText = 'Parse';

    parseBtn.addEventListener( 'click', (e) => {
      e.preventDefault();
      let str = inputStr.value;
      let parse = JSON.parse(str);
      console.log( parse );
    });

    form2.appendChild( inputStr );
    form2.appendChild( parseBtn );

    root.appendChild( form1 );
    root.appendChild( form2 );
    
    document.body.appendChild( root );
});