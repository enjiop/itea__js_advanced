/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

async function getCompanies() {
  const getCompanyResponce = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
  const companies = await getCompanyResponce.json();

  let result = [];

  companies.forEach(element => {
    let {
      company,
      balance,
      registered,
      address
    } = element;

    let companyObj = {
      company,
      balance,
      registered,
      address
    };

    result.push(companyObj);
  });

  return result;
}

document.addEventListener('DOMContentLoaded', () => {
  const root = document.createElement('div');
  root.id = 'root';
  const table = document.createElement('table');
  const tableHeader = document.createElement('tr');

  tableHeader.innerHTML = `
    <thead>
      <th>#</th>
      <th>Company</th>
      <th>Balance</th>
      <th>Показать дату регистрации</th>
      <th>Показать адресс</th>
    </thead>
  `;

  const tBody = document.createElement('tbody');

  let companies = getCompanies();
  companies.then(data => {

    data.forEach((element, index) => {
      let tr = document.createElement('tr');
      tr.innerHTML = `
        <td class="_number">${index + 1}</td>
        <td class="_name">${element.company}</td>
        <td class="_balance">${element.balance}</td>
        <td><button class="_getDate">Показать</button></td>
        <td><button class="_getAdress">Показать</button></td>
      `;

      tBody.appendChild(tr);
    });

    const getDateBtns = document.querySelectorAll('._getDate');
    const getAdressBtns = document.querySelectorAll('._getAdress');
    getDateBtns.forEach(element => {
      element.addEventListener('click', (e) => {
        const crrTd = e.path[1];
        const crrTr = e.path[2];

        let index = +crrTr.querySelector('._number').innerText; 

        crrTd.innerHTML = data[index-1].registered;
      });
    });

    getAdressBtns.forEach(element => {
      element.addEventListener('click', (e) => {
        const crrTd = e.path[1];
        const crrTr = e.path[2];

        let index = +crrTr.querySelector('._number').innerText; 

        crrTd.innerHTML = `${data[index-1].address.city}, ${data[index-1].address.zip}, ${data[index-1].address.city}, ${data[index-1].address.country}, ${data[index-1].address.state}, ${data[index-1].address.street}, ${data[index-1].address.house}`;
      });
    });
  });

  table.appendChild(tableHeader);
  table.appendChild(tBody);
  root.appendChild(table);
  document.body.appendChild(root);
});