/*
172.17.13.181:3003/register/ -> POST
    {
        username: «»,
        email: «»
    }

172.17.13.181:3003/task/ -> POST
    {
        title: «»,
        description: «»,
        user: «»
    }

172.17.13.181:3003/users/ -> GET
172.17.13.181:3003/tasks/ -> GET

*/

fetch('172.17.13.181:3003/register/', {
    method: 'POST',
    body: JSON.stringify({
        username: "name",
        email: "emai@mail.com"
    })
});

fetch('172.17.13.181:3003/task/', {
    method: 'POST',
    body: JSON.stringify({
        title: "title",
        description: "des",
        user: "user"
    })
});

fetch('172.17.13.181:3003/users/', {
    method: 'GET'
});

fetch('172.17.13.181:3003/tasks/', {
    method: 'GET'
});