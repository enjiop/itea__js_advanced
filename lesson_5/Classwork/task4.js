/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    const isMobile = document.innerWidth < 768;

    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/

const charCodeDistance = (firstChar, secondChar) => +firstChar.charCodeAt() - +secondChar.charCodeAt();
const greatestNegative = (arr) => Math.max.apply(null, arr.filter(num => num <= 0));

const firstLocaleLetters = ['A', 'a', 'А', 'а']; // Latin and cyrillic letters
const lastLocaleLetters = ['Z', 'z', 'Я', 'я'];

const findLocale = (char, mode) => {
  let distances = [];
  let localeDistance = 0;
  if (!mode) {
    for (let i = 0; i < firstLocaleLetters.length; i++) {
      distances.push(charCodeDistance(firstLocaleLetters[i], char));
    }
  } else {
    for (let i = 0; i < lastLocaleLetters.length; i++) {
      distances.push(charCodeDistance(char, lastLocaleLetters[i]));
    }
  }
  
  localeDistance = greatestNegative(distances);

  return distances.indexOf(localeDistance); // 0 - latin capital, 1 - latin lowercase, 2 - cyrillic capital, 3 - cyrillic lowercase
};

const encryptCesar = (shiftCount, word) => {
  let wordArr = word.split('');
  let result = [];

  for (let i = 0; i < wordArr.length; i++) {
    if (wordArr[i] !== " " && wordArr[i] !== "." && wordArr[i] !== "," && wordArr[i] !== "!" && wordArr[i] !== ",") {

      let localeIndex = findLocale(wordArr[i]);
      let newCharCode = wordArr[i].charCodeAt() + shiftCount;

      if (newCharCode > lastLocaleLetters[localeIndex].charCodeAt()) {
        let correctChar = firstLocaleLetters[localeIndex].charCodeAt() + charCodeDistance(String.fromCharCode(newCharCode), lastLocaleLetters[localeIndex]) - 1;
        result.push(String.fromCharCode(correctChar));
      } else {
        result.push(String.fromCharCode(newCharCode));
      }
    } else {
      result.push(wordArr[i]);
    }
  }

  return result.join('');
};

const dencryptCesar = (shiftCount, word) => {
  let wordArr = word.split('');
  let result = [];

  for (let i = 0; i < wordArr.length; i++) {
    if (wordArr[i] !== " " && wordArr[i] !== "." && wordArr[i] !== "," && wordArr[i] !== "!" && wordArr[i] !== ",") {

      let localeIndex = findLocale(wordArr[i], true);

      let newCharCode = wordArr[i].charCodeAt() - shiftCount ;

      if (newCharCode < firstLocaleLetters[localeIndex].charCodeAt()) {
        let correctChar = lastLocaleLetters[localeIndex].charCodeAt() - charCodeDistance(firstLocaleLetters[localeIndex], String.fromCharCode(newCharCode)) + 1;
        result.push(String.fromCharCode(correctChar));
      } else {
        result.push(String.fromCharCode(newCharCode));
      }
    } else {
      result.push(wordArr[i]);
    }
  }

  return result.join('');
}

const encryptCesar1 = encryptCesar.bind( null, 1 );
const encryptCesar2 = encryptCesar.bind( null, 2 );
const encryptCesar3 = encryptCesar.bind( null, 3 );
const encryptCesar4 = encryptCesar.bind( null, 4 );
const encryptCesar5 = encryptCesar.bind( null, 5 );

const dencryptCesar1 = dencryptCesar.bind( null, 1 );
const dencryptCesar2 = dencryptCesar.bind( null, 2 );
const dencryptCesar3 = dencryptCesar.bind( null, 3 );
const dencryptCesar4 = dencryptCesar.bind( null, 4 );
const dencryptCesar5 = dencryptCesar.bind( null, 5 );