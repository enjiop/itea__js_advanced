/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/


function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

let Train = {
    name: 'Train',
    speed: 50,
    passCount: 100,
    move: function() {
        this.speed = 50;
        console.log(`Поезд ${this.name} везет ${this.passCount} со скоростью ${this.speed}`);
    },
    stop: function() {
        this.speed = 0;
        console.log(`Поезд ${this.name} остановился. Скорость ${this.speed}`);
    },
    pickUp: function() {
        let x = getRandomIntInclusive(0, 10);
        this.passCount += x;
        console.log(`Подобранно пассажиров ${x}`);
    }
};

Train.move();
Train.stop();
Train.pickUp();
Train.move();