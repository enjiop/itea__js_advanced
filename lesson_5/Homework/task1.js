/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/

function Comment( name, text, avatarURL ) {
  this.name = name;
  this.text = text;
  this.avatarURL = avatarURL ? avatarURL : 'default.png';
  this.likes = 0;
}

let CommentsArray = [];

for( let i = 0; i < 4; i++ ) {
  CommentsArray.push(new Comment( `Comment #${i}`, `This is comment #${i}`, `avatar${i}.png` ));
}

function Render( CommentArr ) {
  let CommentsFeed = document.createElement('div');

  for( let i = 0; i < CommentArr.length; i++ ) {
    let commentWrap = document.createElement('div');
    
    commentWrap.innerHTML = `
      <h2>${CommentArr[i].name}</h2>
      <p>${CommentArr[i].text}</p>
      <img src="${CommentArr[i].avatarURL}"></img>
    `;

    CommentsFeed.appendChild( commentWrap );
  }

  document.body.appendChild( CommentsFeed );
}

Render( CommentsArray );