/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
    
    */
let users = [];

class User {
    constructor(id, login, password) {
        this.id = id;
        this.login = login;
        this.password = password;
        
        users.push( this );

        this.saveToStorage();
    }

    saveToStorage() {
        localStorage.setItem('users', JSON.stringify( users ));
    }
}

const localData = localStorage.getItem( 'users' );

if( localData !== null ) {
    let classData = JSON.parse(localData).map( item => new User(item.id, item.login, item.password) );
    users = classData;
}

console.log( users );

document.addEventListener('DOMContentLoaded', () => {
    const root = document.createElement('div');
    
    const form = document.createElement('form');
    const login = document.createElement('input');
    const pass = document.createElement('input');
    const submit = document.createElement('input');
    const exitBtn = document.createElement('input');
    
    login.name = "login";
    login.placeholder = "Your login";
    login.type = "text";
    
    pass.name = 'password';
    pass.type = 'password';
    pass.placeholder = 'Your password';
    
    submit.name = 'submit';
    submit.type = 'button';
    submit.value = 'Login';
    
    submit.addEventListener('click', (e) => {
        e.preventDefault;
        
        let hasUser = false;
        let hasErr = false; 

        users.forEach( item => {
            if( item.login === login.value && item.password === pass.value ){
                hasUser = true;
                hasErr = false;
            } else if ( item.login === login.value ){
                hasUser = true;
                hasErr = true;
            }
        });

        const message = document.createElement('span');

        if( hasUser && !hasErr ) {
            message.innerText = `Привет, ${login.value}`;

            root.innerHTML = null;
            root.appendChild( message );
            root.appendChild( exitBtn );
        } else if ( hasErr ) {
            message.innerText = 'Вы ввели неправельный пароль! Повторите попытку.'
            root.appendChild(message);
        } else {
            new User( new Date().getTime(), login.value, pass.value );

            root.innerHTML = null;

            message.innerText = `Привет, ${login.value}`;

            localStorage.setItem('currentUser', login.value);

            root.appendChild(message);
            root.appendChild( exitBtn );
        }

        login.value = null;
        pass.value = null;
    });

    exitBtn.type = 'button';
    exitBtn.value = 'Выйти';

    exitBtn.addEventListener('click', () => {
        const currUser = localStorage.getItem('currentUser');
        users = users.filter( item => currUser !== item.login );
        localStorage.setItem('users', JSON.stringify(users));
        root.innerHTML = null;
        root.appendChild( form );

        localStorage.removeItem('currentUser');
    });
    
    form.appendChild( login );
    form.appendChild( pass );
    form.appendChild( submit );
    root.appendChild( form );
    document.body.appendChild( root );
});
