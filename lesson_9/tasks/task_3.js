/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/

let posts = [];

class Post {
    constructor(_id, isActive, title, about, likes, created_at) {
        this._id = _id;
        this.isActive = isActive;
        this.title = title;
        this.about = about;
        this.likes = likes;
        this.created_at = created_at;

        posts.push(this);

        this.saveToStorage();
    }

    saveToStorage() {
        localStorage.setItem('posts', JSON.stringify(posts));
    }

    addLike() {
        this.likes++;

        this.saveToStorage();
    }

    render() {
        const postNode = document.createElement('div');
        postNode.innerHTML = `
            <h2>${this.title}</h2>
            <p>${this.about}</p>
            <button class="likes">${this.likes}</button>
        `;

        document.body.appendChild(postNode);

        let likesBtn = postNode.querySelector('.likes');
        likesBtn.addEventListener('click', (e) => {
            this.addLike();
            e.target.innerText = this.likes;
        });
    }
}

const localData = localStorage.getItem('posts');

if (localData !== null) {
    let classData = JSON.parse(localData).map(item => new Post(item.id, item.isActive, item.title, item.about, item.likes, item.created_at));
    posts = classData;
}

document.addEventListener('DOMContentLoaded', () => {
    posts.forEach(item => item.render());
});