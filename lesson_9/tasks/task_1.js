
/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getColor() {
    let rgb = {}

    rgb.r = getRandomIntInclusive(0, 255);
    rgb.g = getRandomIntInclusive(0, 255);
    rgb.b = getRandomIntInclusive(0, 255);

    return rgb;
}

document.addEventListener('DOMContentLoaded', () => {
    document.body.style.backgroundColor = localStorage.getItem('bgColor');
});

let button = document.createElement('button');

button.innerText = "Random color";

button.addEventListener('click', () => {
    let rgb = getColor();
    let rgbString = `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`;
    document.body.style.backgroundColor = rgbString;

    localStorage.setItem('bgColor', rgbString);
});

document.body.appendChild( button );