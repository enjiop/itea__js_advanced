/*
  Задание:

  Написать класс SuperDude который как аргумент принимает два параметра:
    - Имя
    - Массив суперспособностей которые являются обьектом.

    Модель суперспособности:
      {
        // Имя способности
        name:'Invisibility',
        // Сообщение которое будет выведено когда способность была вызвана
        spell: function(){ return `${this.name} hide from you`}
      }

    В конструкторе, нужно:
    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то
      значение которое мы передали как аргумент.

    - перебрать массив способностей и на каждую из них создать метод для этого
      обьекта, используя поле name как название метода, а spell как то,
      что нужно вернуть в console.log при вызове этого метода.
    - все способности должны быть неизменяемые

    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );
*/

class SuperDude {
  constructor(name, superPowers) {
    Object.defineProperty(this, "name", {
      value: name,
      configurable: false,
      writable: false,
      enumerable: true
    });

    superPowers.forEach(item => {
      Object.defineProperty(this, item.name, {
        value: () => {
          console.log(item.spell());
        },
        configurable: false,
        writable: false,
        enumerable: true
      });
    });
  }
}

class Spell {
  constructor(name, spellFunc) {
    Object.defineProperty(this, "name", {
      value: name,
      configurable: false,
      writable: false,
      enumerable: true
    });

    Object.defineProperty(this, "spell", {
      value: spellFunc,
      configurable: false,
      writable: false,
      enumerable: true
    });
  }
}

let superPowers = [{
    name: 'Invisibility',
    spell: function () {
      return `${this.name} hide from you`
    }
  },
  {
    name: 'superSpeed',
    spell: function () {
      return `${this.name} running from you`
    }
  },
  {
    name: 'superSight',
    spell: function () {
      return `${this.name} see you`
    }
  },
  {
    name: 'superFroze',
    spell: function () {
      return `${this.name} will froze you`
    }
  },
  {
    name: 'superSkin',
    spell: function () {
      return `${this.name} skin is unbreakable`
    }
  },
];

superPowers.push(new Spell('superDuperAwesomeUltimateSpell', function () {
  return `${this.name} opponents can't resist your coolness and give up!`;
}));

let Luther = new SuperDude('Luther', superPowers);
// Тестирование: Методы должны работать и выводить сообщение.
Luther.superSight();
Luther.superSpeed();
Luther.superFroze();
Luther.Invisibility();
Luther.superSkin();
Luther.superDuperAwesomeUltimateSpell();